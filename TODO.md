# Ideas for further expansion

- Switch to ncurses for interaction with the terminal.
- Add line numbers.
- Soft indents.
- Auto indents.
- Add config file support.
- Use more sophisticated data structure for text storage (gap buffers? ropes? something else?).
- Handle non printable characters properly.
