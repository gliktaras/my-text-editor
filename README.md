# My text editor

An exploratory implementation of a text editor in Rust. Based on the `kilo`
editor tutorial, as seen in
https://viewsourcecode.org/snaptoken/kilo/index.html.

Execute `cargo run` in the project's top level directory to build and run the
editor.
