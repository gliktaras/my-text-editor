//! Low level terminal operations.

extern crate libc;
extern crate nix;
extern crate termios;

use self::libc::ioctl;
use self::libc::winsize;
use self::libc::STDIN_FILENO;
use self::libc::STDOUT_FILENO;
use self::libc::TIOCGWINSZ;
use self::nix::unistd::read;
use self::nix::unistd::write;
use self::termios::*;
use std::str;

use base::*;
use error::*;

pub fn read_byte_from_stdin() -> TerminalResult<u8> {
    let mut byte_buf = [0; 1];
    read(STDIN_FILENO, &mut byte_buf)?;
    Ok(byte_buf[0])
}

#[derive(Debug)]
pub struct EscapeSequence {
    pub command: Vec<u8>,
    pub params: Vec<Vec<u8>>,
}

pub enum ReadEscByte {
    Read,
    Skip,
}

impl EscapeSequence {
    pub fn read_from_stdin(read_esc_char: ReadEscByte) -> TerminalResult<EscapeSequence> {
        let mut byte_buf = [0; 1];
        let mut response_buf = Vec::new();
        let mut bytes_read = match read_esc_char {
            ReadEscByte::Read => 0,
            ReadEscByte::Skip => 1,
        };

        let mut command = Vec::new();
        while read(STDIN_FILENO, &mut byte_buf).unwrap_or(0) == 1 {
            bytes_read += 1;
            let b = byte_buf[0];

            if bytes_read == 1 && b != b'\x1b' {
                return Err(TerminalError::NoEscSequence);
            } else if bytes_read == 2 {
                command.push(b);
            }
            if bytes_read <= 2 {
                continue;
            }

            response_buf.push(b);
            if b.is_ascii_alphabetic() || (b.is_ascii_punctuation() && b != b';') {
                break;
            }
        }

        if response_buf.len() == 0 {
            return Err(TerminalError::NoEscSequence);
        }
        command.push(response_buf.pop().unwrap());

        let params = response_buf
            .split(|c| *c == b';')
            .map(<[u8]>::to_vec)
            .collect::<Vec<Vec<u8>>>();

        Ok(EscapeSequence {
            command: command,
            params: params,
        })
    }
}

pub fn enable_terminal_raw_mode() -> TerminalResult<Termios> {
    let orig_termios = Termios::from_fd(STDIN_FILENO)?;
    let mut new_termios = orig_termios;
    new_termios.c_iflag &= !(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
    new_termios.c_oflag &= !OPOST;
    new_termios.c_cflag |= CS8;
    new_termios.c_lflag &= !(ECHO | ICANON | IEXTEN | ISIG);
    new_termios.c_cc[VMIN] = 0;
    new_termios.c_cc[VTIME] = 1;

    tcsetattr(STDIN_FILENO, TCSANOW, &mut new_termios)?;
    Ok(orig_termios)
}

pub fn restore_termios(orig_termios: Termios) {
    write(STDOUT_FILENO, b"\x1b[2J\x1b[H").expect("could not reset screen contents");
    let mut termios_mut = orig_termios;
    tcsetattr(STDIN_FILENO, TCSAFLUSH, &mut termios_mut)
        .expect("could not restore original terminal settings");
}

fn get_cursor_position() -> TerminalResult<TextCoords> {
    write(STDOUT_FILENO, b"\x1b[6n")?; // Query cursor position.
    let response = EscapeSequence::read_from_stdin(ReadEscByte::Read)?;
    assert!(response.command == b"[R");
    assert!(response.params.len() >= 2);

    let row_count_str = str::from_utf8(&response.params[0]).unwrap();
    let row_count = match row_count_str.parse::<i32>() {
        Ok(count) => count,
        Err(err) => {
            return Err(TerminalError::BadResponse(format!(
                "Count not parse row count: {}",
                err
            )))
        }
    };

    let col_count_str = str::from_utf8(&response.params[1]).unwrap();
    let col_count = match col_count_str.parse::<i32>() {
        Ok(count) => count,
        Err(err) => {
            return Err(TerminalError::BadResponse(format!(
                "Count not parse col count: {}",
                err
            )))
        }
    };

    Ok(TextCoords {
        row: row_count,
        col: col_count,
    })
}

pub fn get_terminal_window_size() -> TerminalResult<TextCoords> {
    let mut ws: winsize = winsize {
        ws_row: 0,
        ws_col: 0,
        ws_xpixel: 0,
        ws_ypixel: 0,
    };

    let ioctl_ret = unsafe { ioctl(STDOUT_FILENO, TIOCGWINSZ, &mut ws) };
    if ioctl_ret != -1 && ws.ws_col > 0 {
        return Ok(TextCoords {
            row: ws.ws_row as i32,
            col: ws.ws_col as i32,
        });
    }

    write(STDOUT_FILENO, b"\x1b[999C\x1b[999B")?; // Move cursor bottom-right.
    get_cursor_position()
}

#[derive(Debug)]
pub struct BufferedTerminalWriter {
    buffer: Vec<u8>,
}

impl BufferedTerminalWriter {
    pub fn new() -> BufferedTerminalWriter {
        BufferedTerminalWriter { buffer: Vec::new() }
    }

    pub fn append_byte(&mut self, byte: u8) {
        self.buffer.push(byte);
    }

    pub fn append<B>(&mut self, bytes: B)
    where
        B: Into<Vec<u8>>,
    {
        self.buffer.append(&mut bytes.into());
    }

    pub fn flush(&mut self) -> TerminalResult<()> {
        write(STDOUT_FILENO, &self.buffer)?;
        self.buffer.clear();
        Ok(())
    }
}
