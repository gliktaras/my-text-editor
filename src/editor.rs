//! Editor state.

extern crate libc;
extern crate nix;
extern crate termios;

use std::cmp;
use std::fs::File;
use std::time::Duration;
use std::time::Instant;

use base::*;
use chunk::*;
use content::*;
use error::*;
use input::*;
use terminal::*;

const FORCE_QUIT_COUNT: usize = 4;
const VERSION: &'static str = env!("CARGO_PKG_VERSION");

#[derive(Debug)]
enum EditorState {
    Normal,
    Quit,
}

fn get_format_escape_sequence(token_type: &TokenType, is_search_result: bool) -> String {
    let fg_color = match token_type {
        TokenType::BlockComment => "4",
        TokenType::Char => "5",
        TokenType::LineComment => "4",
        TokenType::Keyword => "2",
        TokenType::Normal => "7",
        TokenType::Number => "1",
        TokenType::Operator => "1",
        TokenType::RustLifetime => "2",
        TokenType::RustMacro => "1",
        TokenType::String => "6",
        TokenType::TypeName => "3",
    };
    match is_search_result {
        false => format!("\x1b[;3{};40m", fg_color),
        true => format!("\x1b[;30;4{}m", fg_color),
    }
}

pub struct Editor {
    state: EditorState,

    filepath: String,
    content: Content,
    is_dirty: bool,

    term_writer: BufferedTerminalWriter,
    window_size: TextCoords,
    render_pos: TextCoords,

    content_size: TextCoords,
    cursor_pos: TextCoords,
    col_offset: i32,
    row_offset: i32,

    status_message: String,
    status_message_time: Instant,
    quit_press_count: usize,
}

impl Editor {
    pub fn new() -> Editor {
        let mut editor = Editor {
            state: EditorState::Normal,
            filepath: String::new(),
            content: Content::new(),
            is_dirty: false,
            term_writer: BufferedTerminalWriter::new(),
            window_size: Default::default(),
            content_size: Default::default(),
            cursor_pos: TextCoords { row: 0, col: 0 },
            render_pos: TextCoords { row: 0, col: 0 },
            col_offset: 0,
            row_offset: 0,
            status_message: String::new(),
            status_message_time: Instant::now(),
            quit_press_count: 0,
        };

        editor.window_size = get_terminal_window_size().expect("count not get window size");
        editor.content_size = editor.window_size.clone();
        editor.content_size.row -= 2;
        editor.set_status_message("HELP: Ctrl-F to find, Ctrl-S to save, Ctrl-Q to quit.");

        editor
    }

    fn set_status_message<S>(&mut self, message: S)
    where
        S: Into<String>,
    {
        self.status_message = message.into();
        self.status_message_time = Instant::now();
    }

    fn write_lines_to_file(&mut self) -> MteResult<()> {
        let mut file = File::create(self.filepath.as_str())?;
        self.content.write(&mut file)?;
        file.sync_all()?;
        Ok(())
    }

    pub fn load_from_file(&mut self, filepath: &str) -> MteResult<()> {
        self.filepath = String::from(filepath);
        self.content = Content::from_reader(File::open(filepath)?, filepath)?;
        self.content.set_filetype_from_filename(&self.filepath);
        self.is_dirty = false;
        Ok(())
    }

    fn save(&mut self) {
        match self.write_lines_to_file() {
            Ok(_) => {
                self.set_status_message("File saved to disk.");
                self.is_dirty = false;
            }
            Err(err) => self.set_status_message(format!("Could not save file: {:?}", err)),
        };
    }

    fn process_normal_input_key(&mut self, key: InputKey) {
        let mut match_vertical_cursor_col = false;
        match key {
            InputKey::CtrlKey(b'q') | InputKey::Null => (),
            _ => self.quit_press_count = 0,
        }

        match key {
            InputKey::Backspace => {
                self.cursor_pos = self.content.delete_byte(&self.cursor_pos);
                self.is_dirty = true;
            }
            InputKey::CtrlKey(b'f') => {
                let prev_render_pos = self.render_pos.clone();
                let prev_cursor_pos = self.cursor_pos.clone();
                let prev_col_offset = self.col_offset.clone();
                let prev_row_offset = self.row_offset.clone();

                match self.get_prompt_input("Find: ", Some(Box::new(Editor::find_prompt_callback)))
                {
                    Some(_) => (),
                    None => {
                        self.render_pos = prev_render_pos;
                        self.cursor_pos = prev_cursor_pos;
                        self.col_offset = prev_col_offset;
                        self.row_offset = prev_row_offset;
                    }
                };
                self.content.clear_search_result();
            }
            InputKey::CtrlKey(b'q') => {
                self.quit_press_count += 1;
                if self.is_dirty && self.quit_press_count < FORCE_QUIT_COUNT {
                    let remaining = FORCE_QUIT_COUNT - self.quit_press_count;
                    self.set_status_message(format!(
                        "WARNING! File has unsaved changes. Press Ctrl-Q {} more times to \
                         quit anyway.",
                        remaining
                    ));
                } else {
                    self.state = EditorState::Quit;
                    return;
                }
            }
            InputKey::CtrlKey(b's') => {
                if self.filepath.is_empty() {
                    match self.get_prompt_input("Save as: ", None) {
                        Some(filepath) => {
                            self.filepath = filepath;
                            self.content.set_filetype_from_filename(&self.filepath);
                            self.is_dirty = false;
                        }
                        None => {
                            self.set_status_message("Save cancelled");
                            return;
                        }
                    }
                }
                self.save();
            }
            InputKey::CursorDown => {
                self.cursor_pos.row += 1;
                match_vertical_cursor_col = true;
            }
            InputKey::CursorLeft => {
                self.cursor_pos.col -= 1;
                if self.cursor_pos.row > 0 && self.cursor_pos.col < 0 {
                    self.cursor_pos.row -= 1;
                    self.cursor_pos.col = i32::max_value();
                }
            }
            InputKey::CursorRight => {
                self.cursor_pos.col += 1;
                if self.cursor_pos.row < (self.content.row_count() - 1) as i32 {
                    let line_len = self
                        .content
                        .row_content_length(self.cursor_pos.row as usize);
                    if (self.cursor_pos.col as usize) > line_len {
                        self.cursor_pos.row += 1;
                        self.cursor_pos.col = 0;
                    }
                }
            }
            InputKey::CursorUp => {
                self.cursor_pos.row -= 1;
                match_vertical_cursor_col = true;
            }
            InputKey::Delete => {
                self.process_normal_input_key(InputKey::CursorRight);
                self.cursor_pos = self.content.delete_byte(&self.cursor_pos);
                self.is_dirty = true;
            }
            InputKey::End => {
                self.cursor_pos.col = self
                    .content
                    .row_content_length(self.cursor_pos.row as usize)
                    as i32;
            }
            InputKey::Enter => {
                self.cursor_pos = self.content.insert_byte(&self.cursor_pos, b'\n');
                self.is_dirty = true;
            }
            InputKey::Home => {
                self.cursor_pos.col = 0;
            }
            InputKey::PageDown => {
                self.cursor_pos.row = self.row_offset + 2 * self.content_size.row - 1;
                match_vertical_cursor_col = true;
            }
            InputKey::PageUp => {
                self.cursor_pos.row = self.row_offset - self.content_size.row;
                match_vertical_cursor_col = true;
            }
            InputKey::RegularKey(c) => {
                self.cursor_pos = self.content.insert_byte(&self.cursor_pos, c);
                self.is_dirty = true;
            }
            InputKey::Tab => {
                self.cursor_pos = self.content.insert_byte(&self.cursor_pos, b'\t');
                self.is_dirty = true;
            }
            _ => (),
        }

        self.cursor_pos.row = clamp(
            0,
            (self.content.row_count() - 1) as i32,
            self.cursor_pos.row,
        );
        if match_vertical_cursor_col {
            self.cursor_pos = self.content.render_to_text_coords(&self.cursor_pos);
        }
        let line_len = self
            .content
            .row_content_length(self.cursor_pos.row as usize);
        self.cursor_pos.col = cmp::min(cmp::max(self.cursor_pos.col, 0), line_len as i32);
    }

    fn find_and_update_cursor(&mut self, op: SearchOp, query: &Vec<u8>) {
        match self.content.search(op, query, &self.cursor_pos) {
            Some(new_pos) => {
                self.cursor_pos = new_pos;
            }
            None => (),
        }
    }

    fn find_prompt_callback(&mut self, key: InputKey, query: &Vec<u8>) {
        match key {
            InputKey::Backspace | InputKey::RegularKey(_) => {
                self.find_and_update_cursor(SearchOp::FindCurrent, query);
            }
            InputKey::CursorDown | InputKey::CursorRight => {
                self.find_and_update_cursor(SearchOp::FindNext, query);
            }
            InputKey::CursorLeft | InputKey::CursorUp => {
                self.find_and_update_cursor(SearchOp::FindPrevious, query);
            }
            _ => (),
        };
    }

    fn get_prompt_input(
        &mut self,
        prompt_message: &str,
        mut keypress_callback: Option<Box<dyn FnMut(&mut Editor, InputKey, &Vec<u8>)>>,
    ) -> Option<String> {
        let mut prompt_value = Vec::new();
        loop {
            let key = read_input_key().expect("could not read input key");
            match key {
                InputKey::Backspace => {
                    prompt_value.pop();
                }
                InputKey::Escape => {
                    self.set_status_message("");
                    return None;
                }
                InputKey::Enter => {
                    self.set_status_message("");
                    return Some(
                        String::from_utf8(prompt_value).expect("could not convert prompt value"),
                    );
                }
                InputKey::RegularKey(c) => {
                    prompt_value.push(c);
                }
                _ => (),
            }
            match keypress_callback {
                Some(ref mut cb_box) => (*cb_box)(self, key, &prompt_value),
                None => (),
            };

            let message = format!(
                "{}{}",
                prompt_message,
                String::from_utf8(prompt_value.clone()).expect("could not convert prompt value")
            );
            self.set_status_message(message);
            self.refresh_screen().expect("could not refresh screen");
        }
    }

    fn draw_rows(&mut self) {
        for i in 0..self.content_size.row {
            let line_number = i + self.row_offset;
            if line_number < (self.content.row_count() as i32) {
                let line_start = cmp::min(
                    self.content.row_render_length(line_number as usize),
                    self.col_offset as usize,
                );
                let start_pos = TextCoords {
                    row: line_number,
                    col: line_start as i32,
                };
                let bytes = self
                    .content
                    .row_render_bytes(&start_pos, self.content_size.col as usize);
                let mut prev_format = get_format_escape_sequence(&TokenType::Normal, false);
                for (j, b) in bytes.iter().enumerate() {
                    let current_pos = TextCoords {
                        row: start_pos.row,
                        col: start_pos.col + j as i32,
                    };
                    let is_search = self.content.is_search_result(&current_pos);
                    let current_format = get_format_escape_sequence(b.token_type(), is_search);
                    if current_format != prev_format {
                        self.term_writer.append(current_format.clone().into_bytes());
                        prev_format = current_format;
                    }
                    self.term_writer.append_byte(b.byte());
                }
                self.term_writer.append("\x1b[m"); // Reset formatting.
            } else if self.filepath.is_empty() && !self.is_dirty && i == self.content_size.row / 3 {
                let mut message = format!("Welcome to My Text self ({})", VERSION);
                let mut padding = (self.content_size.col as i32 - message.len() as i32) / 2;
                if padding < 0 {
                    message.truncate(self.content_size.col as usize);
                    padding = 0;
                }
                if padding > 0 {
                    self.term_writer.append("~");
                    self.term_writer
                        .append(" ".repeat(padding as usize - 1).as_bytes());
                }
                self.term_writer.append(message.into_bytes());
            } else {
                self.term_writer.append("~");
            }
            self.term_writer.append("\x1b[K"); // Clear the rest of the line.
            self.term_writer.append("\r\n");
        }
    }

    fn draw_status_bar(&mut self) {
        self.term_writer.append("\x1b[7m"); // Invert colors.

        let mut bar_right = format!(
            "{} {}/{}",
            self.content.syntax_def().name(),
            self.cursor_pos.row + 1,
            self.content.row_count()
        );
        bar_right.truncate(self.window_size.col as usize);

        let mut filepath = self.filepath.as_str();
        if filepath.is_empty() {
            filepath = "[No name]";
        }

        let mut bar_left = format!("{}", filepath);
        if self.is_dirty {
            bar_left.push_str(" (modified)");
        }
        let bar_left_len = cmp::max(self.window_size.col - bar_right.len() as i32 - 1, 0);
        bar_left.truncate(bar_left_len as usize);

        let padding_size = self.window_size.col as usize - bar_left.len() - bar_right.len();

        self.term_writer.append(bar_left.into_bytes());
        self.term_writer.append(" ".repeat(padding_size).as_bytes());
        self.term_writer.append(bar_right.into_bytes());

        self.term_writer.append("\x1b[m"); // Reset colors.
        self.term_writer.append("\r\n");
    }

    fn draw_status_message(&mut self) {
        self.term_writer.append("\x1b[K");
        let age = Instant::now() - self.status_message_time;
        if age >= Duration::from_secs(5) {
            return;
        }
        let message_end = cmp::min(self.status_message.len(), self.window_size.col as usize);
        self.term_writer
            .append(&self.status_message[0..message_end]);
    }

    fn scroll_viewport(&mut self) {
        self.render_pos = self.content.text_to_render_coords(&self.cursor_pos);

        let bottom_row = self.row_offset + self.content_size.row;
        if self.render_pos.row < self.row_offset {
            self.row_offset -= self.row_offset - self.render_pos.row;
        } else if self.render_pos.row >= bottom_row {
            self.row_offset += self.render_pos.row - bottom_row + 1;
        }

        let right_col = self.col_offset + self.content_size.col;
        if self.render_pos.col < self.col_offset {
            self.col_offset -= self.col_offset - self.render_pos.col;
        } else if self.render_pos.col >= right_col {
            self.col_offset += self.render_pos.col - right_col + 1;
        }
    }

    fn refresh_screen(&mut self) -> MteResult<()> {
        self.term_writer.append("\x1b[?25l"); // Hide cursor.
        self.term_writer.append("\x1b[H"); // Reset cursor position.

        self.scroll_viewport();
        self.draw_rows();
        self.draw_status_bar();
        self.draw_status_message();

        let cursor_x = self.render_pos.row - self.row_offset + 1;
        let cursor_y = self.render_pos.col - self.col_offset + 1;
        self.term_writer
            .append(format!("\x1b[{};{}H", cursor_x, cursor_y).into_bytes()); // Position cursor.

        self.term_writer.append("\x1b[?25h"); // Show cursor.
        match self.term_writer.flush() {
            Ok(_) => Ok(()),
            Err(err) => Err(MteError::TerminalError(err)),
        }
    }

    pub fn event_loop(&mut self) {
        loop {
            match self.state {
                EditorState::Normal => {
                    let key = read_input_key().expect("could not read input key");
                    self.process_normal_input_key(key);
                }
                EditorState::Quit => break,
            }
            self.refresh_screen().expect("could not refresh screen");
        }
    }
}
