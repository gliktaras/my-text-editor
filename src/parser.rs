//! Editor text parser.

use base::*;
use chunk::*;
use std;
use syntax::*;

const TAB_SIZE: usize = 8;

#[derive(Debug)]
struct ParseState<'a> {
    chunk: &'a mut Chunk,
    content_index: usize,
    rendered_index: usize,
    prev_is_separator: bool,
    active_token_type: TokenType,
}

impl<'a> ParseState<'a> {
    fn unparsed_byte(&self, offset: usize) -> u8 {
        *self
            .chunk
            .content()
            .get(self.content_index + offset)
            .unwrap_or(&b'\0')
    }

    fn unparsed_content(&'a self) -> &'a [u8] {
        &self.chunk.content()[self.content_index..]
    }
}

fn get_identifier_length(s: &[u8]) -> usize {
    let mut i = 0;
    while i < s.len() {
        if !s[i].is_ascii_alphabetic() && s[i] != b'_' && (i > 0 && !s[i].is_ascii_digit()) {
            break;
        }
        i += 1;
    }
    i
}

#[derive(Debug)]
pub struct Parser {
    syntax_def: &'static SyntaxDef,
    tab_size: usize,
}

impl Parser {
    pub fn new() -> Parser {
        Parser {
            syntax_def: &DEFAULT_SYNTAX_DEF,
            tab_size: TAB_SIZE,
        }
    }

    pub fn syntax_def(&self) -> &SyntaxDef {
        self.syntax_def
    }

    pub fn set_syntax_def(&mut self, syntax_def: &'static SyntaxDef) {
        self.syntax_def = syntax_def;
    }

    pub fn set_tab_size(&mut self, tab_size: usize) {
        self.tab_size = tab_size;
    }

    pub fn rendered_char_width(&self, byte: &u8, col: usize) -> usize {
        match *byte {
            b'\t' => self.tab_size - (col % self.tab_size),
            _ => 1,
        }
    }

    pub fn is_separator(&self, c: &u8) -> bool {
        *c == b'\0' || self.syntax_def.separator_bytes().contains(c)
    }

    fn render<'a>(&self, parse_state: &'a mut ParseState, length: usize, token_type: TokenType) {
        let length = std::cmp::min(
            length,
            parse_state.chunk.content().len() - parse_state.content_index,
        );
        for _ in 0..length {
            let content_byte = parse_state.unparsed_byte(0);
            let width = self.rendered_char_width(&content_byte, parse_state.rendered_index);
            let fill_byte = match content_byte {
                b'\t' => b' ',
                _ => content_byte,
            };
            for _ in 0..width {
                parse_state
                    .chunk
                    .rendered_mut()
                    .push(RenderableByte::new(fill_byte, token_type.clone()));
            }
            parse_state.content_index += 1;
            parse_state.rendered_index += width;
        }
    }

    fn starts_with_sequence<'a>(
        &self,
        parse_state: &ParseState<'a>,
        sequences: &'a [Vec<u8>],
    ) -> Option<&'a Vec<u8>> {
        for seq in sequences {
            if parse_state.unparsed_content().starts_with(seq.as_slice()) {
                return Some(&seq);
            }
        }
        return None;
    }

    fn starts_with_wrapped_token<'a>(
        &self,
        parse_state: &ParseState<'a>,
        endpoints: &[Endpoints<Vec<u8>>],
    ) -> Option<usize> {
        for entry in endpoints {
            if !parse_state
                .unparsed_content()
                .starts_with(entry.start.as_slice())
            {
                continue;
            }
            let mut len = entry.start.len();
            loop {
                let byte = parse_state.unparsed_byte(len);
                if byte == b'\0' {
                    break;
                } else if parse_state.unparsed_content()[len..].starts_with(entry.end.as_slice()) {
                    len += entry.end.len();
                    break;
                } else if byte == b'\\' {
                    len += 1;
                }
                len += 1;
            }
            return Some(len);
        }
        None
    }

    fn try_parsing_block_comment<'a>(&self, parse_state: &mut ParseState<'a>) -> bool {
        if !self.syntax_def.has_feature(SyntaxFeature::BLOCK_COMMENTS)
            || self.syntax_def.block_comment_endpoints().is_none()
            || (parse_state.active_token_type != TokenType::Normal
                && parse_state.active_token_type != TokenType::BlockComment)
        {
            return false;
        }
        let comment_endpoints = self.syntax_def.block_comment_endpoints().as_ref().unwrap();

        if parse_state.active_token_type == TokenType::Normal {
            if !parse_state
                .unparsed_content()
                .starts_with(comment_endpoints.start.as_slice())
            {
                return false;
            }
            self.render(
                parse_state,
                comment_endpoints.start.len(),
                TokenType::BlockComment,
            );
        }

        parse_state.active_token_type = TokenType::BlockComment;
        while parse_state.content_index < parse_state.chunk.content().len() {
            if parse_state
                .unparsed_content()
                .starts_with(comment_endpoints.end.as_slice())
            {
                self.render(
                    parse_state,
                    comment_endpoints.end.len(),
                    TokenType::BlockComment,
                );
                parse_state.active_token_type = TokenType::Normal;
                break;
            }
            self.render(parse_state, 1, TokenType::BlockComment);
        }
        true
    }

    fn try_parsing_char<'a>(&self, mut parse_state: &mut ParseState<'a>) -> bool {
        if !self.syntax_def.has_feature(SyntaxFeature::CHARS)
            || parse_state.active_token_type != TokenType::Normal
        {
            return false;
        }
        match self.starts_with_wrapped_token(&parse_state, self.syntax_def.char_endpoints()) {
            Some(len) => {
                self.render(&mut parse_state, len, TokenType::Char);
                return true;
            }
            None => (),
        }
        false
    }

    fn try_parsing_keyword<'a>(&self, mut parse_state: &mut ParseState<'a>) -> bool {
        if !self.syntax_def.has_feature(SyntaxFeature::KEYWORDS)
            || !parse_state.prev_is_separator
            || parse_state.active_token_type != TokenType::Normal
        {
            return false;
        }
        match self.starts_with_sequence(&parse_state, self.syntax_def.keywords()) {
            Some(keyword) => {
                if self.is_separator(&parse_state.unparsed_byte(keyword.len())) {
                    self.render(&mut parse_state, keyword.len(), TokenType::Keyword);
                    return true;
                }
            }
            None => (),
        }
        false
    }

    fn try_parsing_line_comment<'a>(&self, mut parse_state: &mut ParseState<'a>) -> bool {
        if !self.syntax_def.has_feature(SyntaxFeature::LINE_COMMENTS)
            || self.syntax_def.line_comment_start().is_none()
            || parse_state.active_token_type != TokenType::Normal
        {
            return false;
        }
        if parse_state
            .unparsed_content()
            .starts_with(self.syntax_def.line_comment_start().as_ref().unwrap())
        {
            self.render(&mut parse_state, usize::max_value(), TokenType::LineComment);
            return true;
        }
        false
    }

    fn try_parsing_number<'a>(&self, mut parse_state: &mut ParseState<'a>) -> bool {
        if !self.syntax_def.has_feature(SyntaxFeature::NUMBERS)
            || !parse_state.prev_is_separator
            || parse_state.active_token_type != TokenType::Normal
            || !parse_state.unparsed_byte(0).is_ascii_digit()
        {
            return false;
        }

        let mut len = 1;
        if parse_state.unparsed_byte(1) == b'x' && parse_state.unparsed_byte(2).is_ascii_digit() {
            len += 2;
        }
        while parse_state.unparsed_byte(len).is_ascii_digit() {
            len += 1;
        }
        self.render(&mut parse_state, len, TokenType::Number);
        return true;
    }

    fn try_parsing_operator<'a>(&self, mut parse_state: &mut ParseState<'a>) -> bool {
        if !self.syntax_def.has_feature(SyntaxFeature::OPERATORS)
            || parse_state.active_token_type != TokenType::Normal
        {
            return false;
        }
        match self.starts_with_sequence(&parse_state, self.syntax_def.operators()) {
            Some(operator) => {
                self.render(&mut parse_state, operator.len(), TokenType::Operator);
                return true;
            }
            None => (),
        }
        false
    }

    fn try_parsing_rust_lifetime<'a>(&self, mut parse_state: &mut ParseState<'a>) -> bool {
        if !self.syntax_def.has_feature(SyntaxFeature::RUST_LIFETIMES)
            || parse_state.unparsed_byte(0) != b'\''
            || parse_state.active_token_type != TokenType::Normal
        {
            return false;
        }
        let len = get_identifier_length(&parse_state.unparsed_content()[1..]) + 1;
        if self.is_separator(&parse_state.unparsed_byte(len)) {
            self.render(&mut parse_state, len, TokenType::RustLifetime);
            return true;
        }
        false
    }

    fn try_parsing_rust_macro<'a>(&self, mut parse_state: &mut ParseState<'a>) -> bool {
        if !self.syntax_def.has_feature(SyntaxFeature::RUST_MACROS)
            || parse_state.active_token_type != TokenType::Normal
            || !parse_state.prev_is_separator
        {
            return false;
        }
        let len = get_identifier_length(parse_state.unparsed_content());
        if parse_state.unparsed_byte(len) == b'!' {
            self.render(&mut parse_state, len + 1, TokenType::RustMacro);
            return true;
        }
        false
    }

    fn try_parsing_string<'a>(&self, mut parse_state: &mut ParseState<'a>) -> bool {
        if !self.syntax_def.has_feature(SyntaxFeature::STRINGS) {
            return false;
        }
        match self.starts_with_wrapped_token(&parse_state, self.syntax_def.string_endpoints()) {
            Some(len) => {
                self.render(&mut parse_state, len, TokenType::String);
                return true;
            }
            None => (),
        }
        false
    }

    fn try_parsing_type_name<'a>(&self, mut parse_state: &mut ParseState<'a>) -> bool {
        if !self.syntax_def.has_feature(SyntaxFeature::TYPE_NAMES)
            || !parse_state.prev_is_separator
            || parse_state.active_token_type != TokenType::Normal
        {
            return false;
        }
        match self.starts_with_sequence(&parse_state, self.syntax_def.type_names()) {
            Some(type_name) => {
                if self.is_separator(&parse_state.unparsed_byte(type_name.len())) {
                    self.render(&mut parse_state, type_name.len(), TokenType::TypeName);
                    return true;
                }
            }
            None => (),
        }
        false
    }

    pub fn parse_line<'a>(&self, chunk: &'a mut Chunk, start_token_type: TokenType) {
        chunk.rendered_mut().clear();
        let mut parse_state: ParseState<'a> = ParseState {
            chunk: chunk,
            content_index: 0,
            rendered_index: 0,
            prev_is_separator: true,
            active_token_type: start_token_type,
        };

        while parse_state.content_index < parse_state.chunk.content().len() {
            if parse_state.content_index > 0 {
                parse_state.prev_is_separator =
                    self.is_separator(&parse_state.chunk.content()[parse_state.content_index - 1]);
            }

            let has_token = self.try_parsing_block_comment(&mut parse_state)
                || self.try_parsing_line_comment(&mut parse_state)
                || self.try_parsing_rust_lifetime(&mut parse_state)
                || self.try_parsing_rust_macro(&mut parse_state)
                || self.try_parsing_keyword(&mut parse_state)
                || self.try_parsing_operator(&mut parse_state)
                || self.try_parsing_type_name(&mut parse_state)
                || self.try_parsing_char(&mut parse_state)
                || self.try_parsing_string(&mut parse_state)
                || self.try_parsing_number(&mut parse_state);
            if !has_token {
                self.render(&mut parse_state, 1, TokenType::Normal);
            }
        }

        parse_state
            .chunk
            .set_end_token_type(parse_state.active_token_type);
    }
}
