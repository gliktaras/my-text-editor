//! Error and result types.

use std::io;

#[derive(Debug)]
pub enum TerminalError {
    BadResponse(String),
    IoError(io::Error),
    NixError(nix::Error),
    NoEscSequence,
}

impl From<io::Error> for TerminalError {
    fn from(error: io::Error) -> TerminalError {
        TerminalError::IoError(error)
    }
}

impl From<nix::Error> for TerminalError {
    fn from(error: nix::Error) -> TerminalError {
        TerminalError::NixError(error)
    }
}

pub type TerminalResult<T> = Result<T, TerminalError>;

#[derive(Debug)]
pub enum MteError {
    IoError(io::Error),
    TerminalError(TerminalError),
}

impl From<io::Error> for MteError {
    fn from(error: io::Error) -> MteError {
        MteError::IoError(error)
    }
}

impl From<TerminalError> for MteError {
    fn from(error: TerminalError) -> MteError {
        MteError::TerminalError(error)
    }
}

pub type MteResult<T> = Result<T, MteError>;
