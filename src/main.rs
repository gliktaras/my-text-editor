extern crate mte;
extern crate simple_signal;

use simple_signal::Signal;
use std::env;
use std::process;

use mte::editor::*;
use mte::terminal::*;

fn main() {
    let args = env::args().collect::<Vec<String>>();

    let orig_termios = enable_terminal_raw_mode().expect("count not enable raw mode");
    simple_signal::set_handler(&[Signal::Int, Signal::Term], move |_signals| {
        restore_termios(orig_termios);
        process::exit(1);
    });

    let mut editor = Editor::new();
    if args.len() > 1 {
        editor
            .load_from_file(&args[1])
            .expect("could not read file");
    }
    editor.event_loop();

    restore_termios(orig_termios);
}
