//! File type syntax definitions.

use base::*;
use std::collections::HashSet;

bitflags! {
    pub struct SyntaxFeature: u16 {
        const NUMBERS = 0x1;
        const CHARS = 0x2;
        const STRINGS = 0x4;
        const LINE_COMMENTS = 0x8;
        const KEYWORDS = 0x10;
        const TYPE_NAMES = 0x20;
        const OPERATORS = 0x40;
        const RUST_LIFETIMES = 0x80;
        const RUST_MACROS = 0x100;
        const BLOCK_COMMENTS = 0x200;
    }
}

#[derive(Debug)]
pub struct SyntaxDef {
    name: String,
    filename_suffixes: Vec<String>,
    features: SyntaxFeature,

    separator_bytes: HashSet<u8>,

    keywords: Vec<Vec<u8>>,
    operators: Vec<Vec<u8>>,
    type_names: Vec<Vec<u8>>,

    line_comment_start: Option<Vec<u8>>,
    block_comment_endpoints: Option<Endpoints<Vec<u8>>>,
    char_endpoints: Vec<Endpoints<Vec<u8>>>,
    string_endpoints: Vec<Endpoints<Vec<u8>>>,
}

impl SyntaxDef {
    pub fn name(&self) -> &str {
        self.name.as_str()
    }

    pub fn filename_matches(&self, filename: &str) -> bool {
        for suffix in &self.filename_suffixes {
            if filename.ends_with(suffix.as_str()) {
                return true;
            }
        }
        false
    }

    pub fn has_feature(&self, feature: SyntaxFeature) -> bool {
        self.features.contains(feature)
    }

    pub fn separator_bytes(&self) -> &HashSet<u8> {
        &self.separator_bytes
    }

    pub fn keywords(&self) -> &Vec<Vec<u8>> {
        &self.keywords
    }

    pub fn operators(&self) -> &Vec<Vec<u8>> {
        &self.operators
    }

    pub fn type_names(&self) -> &Vec<Vec<u8>> {
        &self.type_names
    }

    pub fn line_comment_start(&self) -> &Option<Vec<u8>> {
        &self.line_comment_start
    }

    pub fn block_comment_endpoints(&self) -> &Option<Endpoints<Vec<u8>>> {
        &self.block_comment_endpoints
    }

    pub fn char_endpoints(&self) -> &Vec<Endpoints<Vec<u8>>> {
        &self.char_endpoints
    }

    pub fn string_endpoints(&self) -> &Vec<Endpoints<Vec<u8>>> {
        &self.string_endpoints
    }
}

fn bytes_to_endpoints(start: &[u8], end: &[u8]) -> Endpoints<Vec<u8>> {
    Endpoints {
        start: start.to_vec(),
        end: end.to_vec(),
    }
}

fn bytes_to_set(items: &[u8]) -> HashSet<u8> {
    items.iter().cloned().collect()
}

fn str_to_bytes(items: Vec<&str>) -> Vec<Vec<u8>> {
    items.iter().map(|s| s.as_bytes().to_vec()).collect()
}

fn str_to_strings(items: Vec<&str>) -> Vec<String> {
    items.iter().map(|s| String::from(*s)).collect()
}

lazy_static! {
    pub static ref SYNTAX_DEFS: Vec<SyntaxDef> = vec![
        SyntaxDef {
            name: String::from("txt"),
            filename_suffixes: str_to_strings(vec![".txt"]),
            features: SyntaxFeature::empty(),
            separator_bytes: HashSet::new(),
            keywords: vec![],
            operators: vec![],
            type_names: vec![],
            line_comment_start: None,
            block_comment_endpoints: None,
            char_endpoints: vec![],
            string_endpoints: vec![],
        },
        SyntaxDef {
            name: String::from("rs"),
            filename_suffixes: str_to_strings(vec![".rs"]),
            features: SyntaxFeature::BLOCK_COMMENTS
                | SyntaxFeature::CHARS
                | SyntaxFeature::KEYWORDS
                | SyntaxFeature::LINE_COMMENTS
                | SyntaxFeature::NUMBERS
                | SyntaxFeature::OPERATORS
                | SyntaxFeature::RUST_LIFETIMES
                | SyntaxFeature::RUST_MACROS
                | SyntaxFeature::STRINGS
                | SyntaxFeature::TYPE_NAMES,
            separator_bytes: bytes_to_set(b" \x0a\n\r\t()[]{}<>.,:;%=+-*/&|~"),
            keywords: str_to_bytes(vec![
                "as", "break", "const", "continue", "crate", "else", "enum", "extern", "false",
                "fn", "for", "if", "impl", "in", "let", "loop", "match", "mod", "move", "mut",
                "pub", "ref", "return", "Self", "self", "static", "struct", "super", "trait",
                "true", "type", "unsafe", "use", "where", "while",
            ]),
            operators: str_to_bytes(vec![
                "!", "!=", "%", "%=", "&", "&&", "&=", "*", "*=", "+", "+=", "-", "-=", "->", "::",
                "..", "...", "/", "/=", "<", "<<", "<<=", "<=", "=", "==", "=>", ">", ">=", ">>",
                ">>=", "?", "@", "^", "^=", "|", "|=", "||",
            ]),
            type_names: str_to_bytes(vec![
                "bool", "char", "f32", "f64", "i16", "i32", "i64", "i8", "isize", "str", "u16",
                "u32", "u64", "u8", "usize",
            ]),
            line_comment_start: Some(b"//".to_vec()),
            block_comment_endpoints: Some(bytes_to_endpoints(b"/*", b"*/")),
            char_endpoints: vec![
                bytes_to_endpoints(b"b'", b"'"),
                bytes_to_endpoints(b"'", b"'"),
            ],
            string_endpoints: vec![
                bytes_to_endpoints(b"br\"", b"\""),
                bytes_to_endpoints(b"b\"", b"\""),
                bytes_to_endpoints(b"r\"", b"\""),
                bytes_to_endpoints(b"\"", b"\""),
            ],
        }
    ];
    pub static ref DEFAULT_SYNTAX_DEF: &'static SyntaxDef = &SYNTAX_DEFS[0];
}
