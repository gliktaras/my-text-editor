//! Basic data structures and utilities.

use std;

#[derive(Debug)]
pub struct Endpoints<T> {
    pub start: T,
    pub end: T,
}

#[derive(Clone, Debug, Default)]
pub struct TextCoords {
    pub row: i32,
    pub col: i32,
}

pub fn clamp<T>(min: T, max: T, value: T) -> T
where
    T: std::cmp::Ord,
{
    std::cmp::min(std::cmp::max(value, min), max)
}
