//! Input abstraction layer.

use error::*;
use terminal::*;

#[derive(Debug)]
pub enum InputKey {
    Backspace,
    CtrlKey(u8),
    CursorDown,
    CursorLeft,
    CursorRight,
    CursorUp,
    Delete,
    End,
    Enter,
    Escape,
    Home,
    Null,
    PageDown,
    PageUp,
    RegularKey(u8),
    Tab,
}

pub fn read_input_key() -> MteResult<InputKey> {
    let b = read_byte_from_stdin()?;
    if b == b'\x00' {
        return Ok(InputKey::Null);
    } else if b == b'\x1b' {
        let sequence = match EscapeSequence::read_from_stdin(ReadEscByte::Skip) {
            Ok(s) => s,
            Err(TerminalError::NoEscSequence) => return Ok(InputKey::Escape),
            Err(err) => return Err(MteError::TerminalError(err)),
        };
        let key = match sequence.command.as_slice() {
            b"OF" => InputKey::End,
            b"OH" => InputKey::Home,
            b"[A" => InputKey::CursorUp,
            b"[B" => InputKey::CursorDown,
            b"[C" => InputKey::CursorRight,
            b"[D" => InputKey::CursorLeft,
            b"[F" => InputKey::End,
            b"[H" => InputKey::Home,
            b"[~" => {
                assert!(sequence.params.len() >= 1);
                match sequence.params[0].as_slice() {
                    b"1" => InputKey::Home,
                    b"3" => InputKey::Delete,
                    b"4" => InputKey::End,
                    b"5" => InputKey::PageUp,
                    b"6" => InputKey::PageDown,
                    b"7" => InputKey::Home,
                    b"8" => InputKey::End,
                    _ => InputKey::Escape,
                }
            }
            _ => InputKey::Escape,
        };
        return Ok(key);
    } else if b == b'\x08' || b == b'\x7f' {
        return Ok(InputKey::Backspace);
    } else if b == b'\r' {
        return Ok(InputKey::Enter);
    } else if b == b'\t' {
        return Ok(InputKey::Tab);
    } else if b.is_ascii_control() {
        return Ok(InputKey::CtrlKey(b + 0x60));
    } else {
        return Ok(InputKey::RegularKey(b));
    }
}
