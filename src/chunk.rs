//! Data structure for a single chunk of editor content.

use std::cmp;

#[derive(Debug)]
pub struct RenderableByte {
    byte: u8,
    token_type: TokenType,
}

impl RenderableByte {
    pub fn new(byte: u8, token_type: TokenType) -> RenderableByte {
        RenderableByte {
            byte: byte,
            token_type: token_type,
        }
    }

    pub fn byte(&self) -> u8 {
        self.byte
    }

    pub fn token_type(&self) -> &TokenType {
        &self.token_type
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum TokenType {
    BlockComment,
    Char,
    Keyword,
    Normal,
    Number,
    Operator,
    RustLifetime,
    RustMacro,
    LineComment,
    String,
    TypeName,
}

#[derive(Debug)]
pub struct Chunk {
    content: Vec<u8>,
    rendered: Vec<RenderableByte>,
    end_token_type: TokenType,
}

impl Chunk {
    pub fn new() -> Chunk {
        Chunk::with_content(Vec::new())
    }

    pub fn with_content(content: Vec<u8>) -> Chunk {
        Chunk {
            content: content,
            rendered: Vec::new(),
            end_token_type: TokenType::Normal,
        }
    }

    pub fn content(&self) -> &Vec<u8> {
        &self.content
    }

    pub fn content_mut(&mut self) -> &mut Vec<u8> {
        &mut self.content
    }

    pub fn rendered(&self) -> &Vec<RenderableByte> {
        &self.rendered
    }

    pub fn rendered_mut(&mut self) -> &mut Vec<RenderableByte> {
        &mut self.rendered
    }

    pub fn end_token_type(&self) -> &TokenType {
        &self.end_token_type
    }

    pub fn set_end_token_type(&mut self, token_type: TokenType) {
        self.end_token_type = token_type;
    }

    pub fn split_off_content(&mut self, text_col: usize) -> Chunk {
        Chunk {
            content: self.content.split_off(text_col),
            rendered: Vec::new(),
            end_token_type: TokenType::Normal,
        }
    }

    pub fn append_content(&mut self, mut chunk: Chunk) {
        self.content.append(&mut chunk.content);
    }

    pub fn find_first(&self, query: &Vec<u8>, start_text_col: usize) -> Option<usize> {
        let end_col = cmp::max(self.content.len(), query.len()) - query.len();
        for col in start_text_col..end_col {
            let mut has_match = true;
            for (i, byte) in query.iter().enumerate() {
                if *byte != self.content[col + i] {
                    has_match = false;
                    break;
                }
            }
            if has_match {
                return Some(col);
            }
        }
        return None;
    }

    pub fn find_last(&self, query: &Vec<u8>, end_text_col: usize) -> Option<usize> {
        let end_col = cmp::max(end_text_col, query.len()) - query.len();
        for col in (0..end_col).rev() {
            let mut has_match = true;
            for (i, byte) in query.iter().enumerate() {
                if *byte != self.content[col + i] {
                    has_match = false;
                    break;
                }
            }
            if has_match {
                return Some(col);
            }
        }
        return None;
    }
}
