//! Data structures for editor's content.

use base::*;
use chunk::*;
use error::*;
use parser::*;
use std;
use syntax::*;

#[derive(Debug)]
pub enum SearchOp {
    FindCurrent,
    FindNext,
    FindPrevious,
}

#[derive(Debug)]
pub struct Content {
    chunks: Vec<Chunk>,
    parser: Parser,
    search_query_row: usize,
    search_query_col: usize,
    search_query_len: usize,
}

impl Content {
    pub fn new() -> Content {
        Content {
            chunks: vec![Chunk::new()],
            parser: Parser::new(),
            search_query_row: 0,
            search_query_col: 0,
            search_query_len: 0,
        }
    }

    pub fn from_reader<R>(reader: R, filename: &str) -> MteResult<Content>
    where
        R: std::io::Read,
    {
        let mut chunks = Vec::new();

        let mut current_line = Vec::new();
        for byte in reader.bytes() {
            let byte = byte?;
            if byte == b'\n' {
                chunks.push(Chunk::with_content(current_line.clone()));
                current_line.clear();
            } else {
                current_line.push(byte);
            }
        }
        if !current_line.is_empty() {
            chunks.push(Chunk::with_content(current_line));
        }

        let mut content = Content {
            chunks: chunks,
            parser: Parser::new(),
            search_query_row: 0,
            search_query_col: 0,
            search_query_len: 0,
        };
        content.set_filetype_from_filename(filename);
        content.update_all_rendered();

        Ok(content)
    }

    fn update_rendered(&mut self, sorted_rows: &[usize]) {
        let mut next_index = 0;
        for row in sorted_rows {
            if next_index > *row {
                continue;
            }
            next_index = *row;

            while next_index < self.chunks.len() {
                let start_token_type = if next_index > 0 {
                    self.chunks[next_index - 1].end_token_type().clone()
                } else {
                    TokenType::Normal
                };
                let chunk = &mut self.chunks[next_index];

                let old_end_token_type = chunk.end_token_type().clone();
                self.parser.parse_line(chunk, start_token_type);

                if *chunk.end_token_type() == old_end_token_type {
                    break;
                }
                next_index += 1;
            }
        }
    }

    fn update_all_rendered(&mut self) {
        let indexes = (0..self.chunks.len()).collect::<Vec<usize>>();
        self.update_rendered(indexes.as_slice());
    }

    pub fn row_count(&self) -> usize {
        self.chunks.len()
    }

    pub fn row_content_length(&self, row: usize) -> usize {
        self.chunks[row].content().len()
    }

    pub fn row_render_length(&self, row: usize) -> usize {
        self.chunks[row].rendered().len()
    }

    pub fn row_render_bytes(&self, render_pos: &TextCoords, len: usize) -> &[RenderableByte] {
        let row_len = self.row_render_length(render_pos.row as usize);
        let start = clamp(0, row_len, render_pos.col as usize);
        let end = clamp(0, row_len, (render_pos.col as usize) + len);
        &self.chunks[render_pos.row as usize].rendered()[start..end]
    }

    pub fn syntax_def(&self) -> &SyntaxDef {
        self.parser.syntax_def()
    }

    pub fn set_filetype_from_filename(&mut self, filename: &str) {
        let mut syntax_def: &SyntaxDef = &DEFAULT_SYNTAX_DEF;
        for def in SYNTAX_DEFS.iter() {
            if def.filename_matches(filename) {
                syntax_def = def;
                break;
            }
        }
        self.parser.set_syntax_def(syntax_def);
        self.update_all_rendered();
    }

    fn text_to_render_col(&self, content_line: &Vec<u8>, text_col: i32) -> i32 {
        let mut render_col = 0;
        for (i, byte) in content_line.iter().enumerate() {
            if (i as i32) >= text_col {
                break;
            }
            render_col += self.parser.rendered_char_width(byte, render_col);
        }
        render_col as i32
    }

    pub fn text_to_render_coords(&self, text_pos: &TextCoords) -> TextCoords {
        let content_line = &self.chunks[text_pos.row as usize].content();
        TextCoords {
            row: text_pos.row,
            col: self.text_to_render_col(content_line, text_pos.col),
        }
    }

    pub fn render_to_text_coords(&self, render_pos: &TextCoords) -> TextCoords {
        let content_line = &self.chunks[render_pos.row as usize].content();
        let render_len = clamp(
            0,
            self.row_render_length(render_pos.row as usize),
            render_pos.col as usize,
        );
        let mut text_col = 0;
        let mut render_col = 0;

        for byte in content_line.iter() {
            render_col += self.parser.rendered_char_width(byte, render_col);
            if render_col > render_len {
                break;
            }
            text_col += 1;
        }
        TextCoords {
            row: render_pos.row,
            col: text_col,
        }
    }

    pub fn insert_byte(&mut self, text_pos: &TextCoords, byte: u8) -> TextCoords {
        let mut new_pos = text_pos.clone();

        if byte == b'\n' {
            new_pos.row += 1;
            new_pos.col = 0;

            let new_chunk =
                self.chunks[text_pos.row as usize].split_off_content(text_pos.col as usize);
            self.chunks.insert((text_pos.row + 1) as usize, new_chunk);
            self.update_rendered(&[text_pos.row as usize, (text_pos.row + 1) as usize]);
        } else {
            new_pos.col += 1;
            self.chunks[text_pos.row as usize]
                .content_mut()
                .insert(text_pos.col as usize, byte);
            self.update_rendered(&[text_pos.row as usize]);
        }

        new_pos
    }

    pub fn delete_byte(&mut self, text_pos: &TextCoords) -> TextCoords {
        let mut new_pos = text_pos.clone();

        if text_pos.col == 0 {
            if text_pos.row > 0 {
                new_pos.row -= 1;
                new_pos.col = self.chunks[new_pos.row as usize].content().len() as i32;

                let suffix_chunk = self.chunks.remove((new_pos.row + 1) as usize);
                self.chunks[new_pos.row as usize].append_content(suffix_chunk);
                self.update_rendered(&[new_pos.row as usize]);
            }
        } else {
            new_pos.col -= 1;
            self.chunks[new_pos.row as usize]
                .content_mut()
                .remove(new_pos.col as usize);
            self.update_rendered(&[new_pos.row as usize]);
        }

        new_pos
    }

    fn mark_search_result(&mut self, row: usize, col: usize, query_len: usize) {
        self.search_query_row = row;
        self.search_query_col =
            self.text_to_render_col(self.chunks[row].content(), col as i32) as usize;
        self.search_query_len = self
            .text_to_render_col(self.chunks[row].content(), (col + query_len) as i32) as usize
            - self.search_query_col;
    }

    pub fn is_search_result(&self, render_pos: &TextCoords) -> bool {
        render_pos.row == self.search_query_row as i32
            && render_pos.col >= self.search_query_col as i32
            && render_pos.col < (self.search_query_col + self.search_query_len) as i32
    }

    pub fn clear_search_result(&mut self) {
        self.search_query_len = 0;
    }

    pub fn search(
        &mut self,
        op: SearchOp,
        query: &Vec<u8>,
        start_pos: &TextCoords,
    ) -> Option<TextCoords> {
        match op {
            SearchOp::FindCurrent | SearchOp::FindNext => {
                let start_row = start_pos.row as usize;
                let mut start_col = match op {
                    SearchOp::FindCurrent => start_pos.col as usize,
                    SearchOp::FindNext => (start_pos.col + 1) as usize,
                    _ => panic!("invalid SearchOp"),
                };
                let end_row = self.chunks.len();

                for row in start_row..end_row {
                    if row != start_row {
                        start_col = 0;
                    }
                    match self.chunks[row].find_first(query, start_col) {
                        Some(query_pos) => {
                            self.mark_search_result(row, query_pos, query.len());
                            return Some(TextCoords {
                                row: row as i32,
                                col: query_pos as i32,
                            });
                        }
                        None => (),
                    };
                }
            }
            SearchOp::FindPrevious => {
                let end_row = (start_pos.row + 1) as usize;
                let mut end_col = start_pos.col as usize;

                for row in (0..end_row).rev() {
                    if row != end_row - 1 {
                        end_col = self.chunks[row].content().len();
                    }
                    match self.chunks[row].find_last(query, end_col) {
                        Some(query_pos) => {
                            self.mark_search_result(row, query_pos, query.len());
                            return Some(TextCoords {
                                row: row as i32,
                                col: query_pos as i32,
                            });
                        }
                        None => (),
                    };
                }
            }
        }
        None
    }

    pub fn write<W>(&self, writer: &mut W) -> MteResult<()>
    where
        W: std::io::Write,
    {
        for chunk in &self.chunks {
            writer.write_all(chunk.content())?;
            writer.write_all(b"\n")?;
        }
        Ok(())
    }
}
